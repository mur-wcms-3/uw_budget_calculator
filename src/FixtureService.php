<?php
// phpcs:ignoreFile

namespace Drupal\uw_budget_calculator;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Fixture service for loading pre-created entities.
 */
class FixtureService {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * Populates the default values for Programs entities.
   */
  public function createDefaultPrograms() {
    // Load systems array to process.
    $programs = $this->getPrograms();

    $store = $this->entityTypeManager->getStorage('budget_program_entity');

    // Loop through each of our program records.
    foreach ($programs as [$name, $id, $domestic, $international,
      $incidental, $coop, $books, $notes, $last_updated,
    ]) {
      // Check if the program already exists, as it needs to be unique.
      $exists = $this->entityTypeManager->getStorage('budget_program_entity')->getQuery()
        ->condition('name', $name)
        ->execute();

      if (!empty($exists)) {

        $this->messenger->addWarning('"' . $name . '" already exists. Skipped.');

      }
      else {
        // Create the program entity.
        $store->create([
          'name' => $name,
          'field_domestic_tuition' => $domestic,
          'field_international_tuition' => $international,
          'field_incidental' => $incidental,
          'field_co_op_fee' => $coop,
          'field_book_costs' => $books,
          'field_notes' => $notes,
          'weight' => 0,
        ],
              )->save();

        $this->messenger->addStatus('"' . $name . '" created.');
      }
    }
  }

  /**
   * Get program values.
   */
  private function getPrograms() {
    // phpcs:disable
    // program_name, program_id, domestic_tuition, international_tuition,
    // incidental, coop_fee, book_costs, Notes, last updated.
    return [
      ["Health Sciences", "1", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Kinesiology", "2", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Recreation and Leisure Studies", "3", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Accounting and Financial Management", "4", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Honours Arts and Business", "5", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Honours Arts", "8", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Social Development Studies", "10", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Architecture", "12", "12000", "61000", "0", "0", "4100.00", "", "july 5 2021"],
      ["Chemical Engineering", "13", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Civil Engineering", "14", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Computer Engineering", "15", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Electrical Engineering", "16", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Environmental Engineering", "17", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Geological Engineering", "18", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Management Engineering", "19", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Mechanical Engineering", "20", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Mechatronics Engineering", "21", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Nanotechnology Engineering", "22", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Systems Design Engineering", "23", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Software Engineering", "24", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Environment and Business", "25", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Environment,Resources and Sustainability", "26", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Geography and Aviation", "27", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Geography and Environmental Management", "28", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Geomatics", "29", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["International Development", "30", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Knowledge Integration", "31", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Planning", "32", "8000", "42000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Business Administration (Laurier) and Computer Science (Waterloo) Double Degree", "34", "15000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Business Administration (Laurier) and Mathematics (Waterloo) Double Degree", "35", "12000", "48000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Computer Science", "37", "15000", "63000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Computing and Financial Management", "38", "8000", "48000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Mathematics", "39", "8000", "48000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Mathematics/Business Administration", "40", "8000", "48000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Mathematics/Chartered Professional Accountancy", "41", "8000", "47000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Mathematics/Financial Analysis and Risk Management", "42", "12000", "48000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Biotechnology/Chartered Professional Accountancy", "43", "8000", "45000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Environmental Science", "45", "8000", "44000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Honours Science", "46", "8000", "44000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Life Sciences", "47", "8000", "44000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Physical Sciences", "48", "8000", "44000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Science and Aviation", "49", "8000", "44000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Science and Business", "50", "0", "0", "0", "0", "2100.00", "", "july 5 2021"],
      ["Global Business and Digital Arts", "53", "8000", "43000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Public Health", "54", "16000", "63000", "0", "0", "2100.00", "", "july 5 2021"],
      ["Biomedical Engineering", "55", "16000", "63000", "0", "739", "2100.00", "Co-op fee starts in 1A", "july 5 2021"],
      ["Architectural Engineering", "60", "16850", "54600", "0", "0", "2100.00", "", "july 5 2021"],
      ["Climate and Environmental Change", "61", "8000", "43000", "0", "0", "2100.00", "", "July 2 2001"],
      ["Sustainability and Financial Management", "62", "8000", "43000", "0", "0", "2100.00", "", "july 7 2021"],
    ];
    // phpcs:enable
  }

  /**
   * Populates the default values for Mealplans entities.
   */
  public function createDefaultMealplans() {
    // Load systems array to process.
    $mealplans = $this->getMealplans();

    $store = $this->entityTypeManager->getStorage('budget_mealplan_entity');

    // Loop through each of our program records.
    foreach ($mealplans as [$id, $name, $price, $notes, $last_updated]) {
      // Check if the program already exists, as it needs to be unique.
      $exists = $this->entityTypeManager->getStorage('budget_mealplan_entity')->getQuery()
        ->condition('name', $name)
        ->execute();

      if (!empty($exists)) {

        $this->messenger->addWarning('"' . $name . '" already exists. Skipped.');

      }
      else {
        // Create the program entity.
        $store->create([
          'name' => $name,
          'field_price' => $price,
          'field_notes' => $notes,
          'weight' => $id,
        ]
              )->save();

        $this->messenger->addStatus('"' . $name . '" created.');
      }
    }
  }

  /**
   * Get mealplan values.
   */
  private function getMealplans() {
    // phpcs:disable
    // mealplan_id, name, price, notes, last_updated.
    return [
      ["1", "Lite", "5200.00", "Village meal plans", "june 11 2021"],
      ["2", "Average", "5600.00", "Village meal plans", "june 11 2021"],
      ["3", "Hearty", "6000.00", "Village meal plans", "june 11 2021"],
      ["4", "Casual", "2980.00", "Suite-style residence", "june 11 2021"],
      ["5", "Saver", "3610.00", "Suite-style residence", "june 11 2021"],
      ["6", "Super Saver", "4120.00", "Suite-style residence", "june 11 2021"],
      ["7", "Meal plan included", "0.00", "University colleges", "june 15 2020"],
      ["8", "None", "0.00", NULL, "june 15 2020"],
    ];
    // phpcs:enable
  }

  /**
   * Populates the default values for Residences entities.
   */
  public function createDefaultResidences() {
    // Load residence array to process.
    $residences = $this->getResidences();

    $store = $this->entityTypeManager->getStorage('budget_residence_entity');

    // Loop through each of our residence records.
    foreach ($residences as [$id, $name, $price, $last_updated, $type]) {
      // Check if the residence already exists, as it needs to be unique.
      $exists = $this->entityTypeManager->getStorage('budget_residence_entity')->getQuery()
        ->condition('name', $name)
        ->execute();

      if (!empty($exists)) {

        $this->messenger->addWarning('"' . $name . '" already exists. Skipped.');

      }
      else {
        // Create the program entity.
        $store->create([
          'name' => $name,
          'field_price' => $price,
          'weight' => $type,
        ]
              )->save();

        $this->messenger->addStatus('"' . $name . '" created.');
      }
    }
  }

  /**
   * Gets residence values.
   */
  private function getResidences() {
    // phpcs:disable
    // residence_id, name, price, last_updated, type.
    return [
      ["3", "Interconnecting (semi-private) room – Village 1 or Ron Eydt Village", "6392.00", "June 11, 2021", "0"],
      ["18", "Single room – Claudette Millar Hall", "7001.00", "June 11, 2021", "1"],
      ["17", "Single room – Columbia Lake Village-South (Four-bedroom townhouse)", "6386.00", "June 11, 2021", "1"],
      ["13", "Single room – Mackenzie King Village (Four-bedroom suite)", "7995.00", "June 11, 2021", "1"],
      ["15", "Single room – UW Place (Three- or four-bedroom suite)", "6837.00", "June 11, 2021", "1"],
      ["14", "Single room – UW Place (Two-bedroom suite)", "7224.00", "June 11, 2021", "1"],
      ["1", "Single room – Village 1 or Ron Eydt Village", "6697.00", "June 11, 2021", "1"],
      ["9", "Single room – St. Jerome's University", "13299.00", "June 11, 2021", "2"],
      ["11", "Single room – St. Paul's University College", "13098.00", "June 11, 2021", "2"],
      ["19", "Double room (semi-private) – Claudette Millar Hall", "7346.00", "June 11, 2021", "3"],
      ["16", "Double room – UW Place", "7224.00", "June 11, 2021", "3"],
      ["2", "Double room – Village 1 or Ron Eydt Village", "6008.00", "June 11, 2021", "3"],
      ["4", "Double room – Conrad Grebel University College", "11860.00", "June 11, 2021", "4"],
      ["8", "Double room – Renison University College", "12870.00", "June 11, 2021", "4"],
      ["10", "Double room – St. Jerome's University", "12499.00", "June 11, 2021", "4"],
      ["12", "Double room – St. Paul's University College", "12007.00", "June 11, 2021", "4"],
    ];
    // phpcs:enable
  }

  /**
   * Creates default relationships.
   */
  public function createDefaultRelationships() {

    $relationships = $this->getRelationships();

    // Loop through each meal plan.
    foreach ($relationships as $plan => $residences) {
      $plan_id = $this->entityTypeManager->getStorage('budget_mealplan_entity')->getQuery()
        ->condition('name', $plan)
        ->execute();

      // Turns the array into a single id int.
      $plan_id = reset($plan_id);

      $entity = $this->entityTypeManager->getStorage('budget_mealplan_entity')->load($plan_id);

      $this->messenger->addStatus('Creating relationships for "' . $entity->name->value . '".');

      // Initialize array for ids that will get assigned to the mealplan.
      $res_ids = [];

      // Add each residence to the meal plan relationship.
      foreach ($residences as $residence) {
        // Find the residence IDs.
        $res_id = $this->entityTypeManager->getStorage('budget_residence_entity')->getQuery()
          ->condition('name', $residence)
          ->execute();

        $this->messenger->addStatus('Adding "' . $residence . '" to "' . $plan . '"');

        // Add to the id array.
        $res_ids[] = reset($res_id);
      }

      // Add the found residence IDs to the mealplan relationship.
      $entity->set('field_residences', $res_ids);
      $entity->save();
    }
  }

  /**
   * Define the mealplan/residence relationships to populate.
   */
  private function getRelationships() {
    // phpcs:disable
    return [
      'Lite' => [
        'Single room – Village 1 or Ron Eydt Village',
        'Single room – Claudette Millar Hall',
        'Interconnecting (semi-private) room – Village 1 or Ron Eydt Village',
        'Double room (semi-private) – Claudette Millar Hall',
        'Double room – Village 1 or Ron Eydt Village',
      ],
      'Average' => [
        'Single room – Village 1 or Ron Eydt Village',
        'Single room – Claudette Millar Hall',
        'Interconnecting (semi-private) room – Village 1 or Ron Eydt Village',
        'Double room (semi-private) – Claudette Millar Hall',
        'Double room – Village 1 or Ron Eydt Village',
      ],
      'Hearty' => [
        'Single room – Village 1 or Ron Eydt Village',
        'Single room – Claudette Millar Hall',
        'Interconnecting (semi-private) room – Village 1 or Ron Eydt Village',
        'Double room (semi-private) – Claudette Millar Hall',
        'Double room – Village 1 or Ron Eydt Village',
      ],
      'Casual' => [
        'Single room – Columbia Lake Village-South (Four-bedroom townhouse)',
        'Single room – Mackenzie King Village (Four-bedroom suite)',
        'Single room – UW Place (Two-bedroom suite)',
        'Single room – UW Place (Three- or four-bedroom suite)',
        'Double room – UW Place',
      ],
      'Saver' => [
        'Single room – Columbia Lake Village-South (Four-bedroom townhouse)',
        'Single room – Mackenzie King Village (Four-bedroom suite)',
        'Single room – UW Place (Two-bedroom suite)',
        'Single room – UW Place (Three- or four-bedroom suite)',
        'Double room – UW Place',
      ],
      'Super Saver' => [
        'Single room – Columbia Lake Village-South (Four-bedroom townhouse)',
        'Single room – Mackenzie King Village (Four-bedroom suite)',
        'Single room – UW Place (Two-bedroom suite)',
        'Single room – UW Place (Three- or four-bedroom suite)',
        'Double room – UW Place',
      ],
      'Meal plan included' => [
        "Double room – Conrad Grebel University College",
        "Single room – St. Paul's University College",
        "Double room – St. Jerome's University",
        "Single room – St. Jerome's University",
        "Double room – Renison University College",
        "Double room – St. Paul's University College",
      ],
      'None' => [
        'Single room – Columbia Lake Village-South (Four-bedroom townhouse)',
        'Single room – Mackenzie King Village (Four-bedroom suite)',
        'Single room – UW Place (Two-bedroom suite)',
        'Single room – UW Place (Three- or four-bedroom suite)',
        'Double room – UW Place',
      ],
    ];
    // phpcs:enable
  }

}
