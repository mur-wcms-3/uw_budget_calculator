<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Budget Mealplan entities.
 *
 * @ingroup uw_budget_calculator
 */
interface BudgetMealplanEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Budget Mealplan name.
   *
   * @return string
   *   Name of the Budget Mealplan.
   */
  public function getName();

  /**
   * Sets the Budget Mealplan name.
   *
   * @param string $name
   *   The Budget Mealplan name.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetMealplanEntityInterface
   *   The called Budget Mealplan entity.
   */
  public function setName($name);

  /**
   * Gets the Budget Mealplan creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Budget Mealplan.
   */
  public function getCreatedTime();

  /**
   * Sets the Budget Mealplan creation timestamp.
   *
   * @param int $timestamp
   *   The Budget Mealplan creation timestamp.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetMealplanEntityInterface
   *   The called Budget Mealplan entity.
   */
  public function setCreatedTime($timestamp);

}
