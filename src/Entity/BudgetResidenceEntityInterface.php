<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Budget Residence entities.
 *
 * @ingroup uw_budget_calculator
 */
interface BudgetResidenceEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Budget Residence name.
   *
   * @return string
   *   Name of the Budget Residence.
   */
  public function getName();

  /**
   * Sets the Budget Residence name.
   *
   * @param string $name
   *   The Budget Residence name.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetResidenceEntityInterface
   *   The called Budget Residence entity.
   */
  public function setName($name);

  /**
   * Gets the Budget Residence creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Budget Residence.
   */
  public function getCreatedTime();

  /**
   * Sets the Budget Residence creation timestamp.
   *
   * @param int $timestamp
   *   The Budget Residence creation timestamp.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetResidenceEntityInterface
   *   The called Budget Residence entity.
   */
  public function setCreatedTime($timestamp);

}
