<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Budget Program entity.
 *
 * @ingroup uw_budget_calculator
 *
 * @ContentEntityType(
 *   id = "budget_program_entity",
 *   label = @Translation("Budget Program"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_budget_calculator\BudgetProgramEntityListBuilder",
 *     "views_data" = "Drupal\uw_budget_calculator\Entity\BudgetProgramEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\uw_budget_calculator\Form\BudgetProgramEntityForm",
 *       "add" = "Drupal\uw_budget_calculator\Form\BudgetProgramEntityForm",
 *       "edit" = "Drupal\uw_budget_calculator\Form\BudgetProgramEntityForm",
 *       "delete" = "Drupal\uw_budget_calculator\Form\BudgetProgramEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_budget_calculator\BudgetProgramEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\uw_budget_calculator\BudgetProgramEntityAccessControlHandler",
 *   },
 *   base_table = "budget_program_entity",
 *   translatable = FALSE,
 *   admin_permission = "administer budget program entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *     "weight" = "weight",
 *   },
 *   links = {
 *     "canonical" = "/admin/mur/budget_calculator/budget_program_entity/{budget_program_entity}",
 *     "add-form" = "/admin/mur/budget_calculator/budget_program_entity/add",
 *     "edit-form" = "/admin/mur/budget_calculator/budget_program_entity/{budget_program_entity}/edit",
 *     "delete-form" = "/admin/mur/budget_calculator/budget_program_entity/{budget_program_entity}/delete",
 *     "collection" = "/admin/mur/budget_calculator/budget_program_entity",
 *   },
 *   field_ui_base_route = "budget_program_entity.settings"
 * )
 */
class BudgetProgramEntity extends ContentEntityBase implements BudgetProgramEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Program Name'))
      ->setDescription(t('The name of the Program.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
