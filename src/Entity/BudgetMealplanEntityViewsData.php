<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Budget Mealplan entities.
 */
class BudgetMealplanEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
