<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Budget Program entities.
 *
 * @ingroup uw_budget_calculator
 */
interface BudgetProgramEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Budget Program name.
   *
   * @return string
   *   Name of the Budget Program.
   */
  public function getName();

  /**
   * Sets the Budget Program name.
   *
   * @param string $name
   *   The Budget Program name.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetProgramEntityInterface
   *   The called Budget Program entity.
   */
  public function setName($name);

  /**
   * Gets the Budget Program creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Budget Program.
   */
  public function getCreatedTime();

  /**
   * Sets the Budget Program creation timestamp.
   *
   * @param int $timestamp
   *   The Budget Program creation timestamp.
   *
   * @return \Drupal\uw_budget_calculator\Entity\BudgetProgramEntityInterface
   *   The called Budget Program entity.
   */
  public function setCreatedTime($timestamp);

}
