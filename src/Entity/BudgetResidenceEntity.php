<?php

namespace Drupal\uw_budget_calculator\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Budget Residence entity.
 *
 * @ingroup uw_budget_calculator
 *
 * @ContentEntityType(
 *   id = "budget_residence_entity",
 *   label = @Translation("Budget Residence"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_budget_calculator\BudgetResidenceEntityListBuilder",
 *     "views_data" = "Drupal\uw_budget_calculator\Entity\BudgetResidenceEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\uw_budget_calculator\Form\BudgetResidenceEntityForm",
 *       "add" = "Drupal\uw_budget_calculator\Form\BudgetResidenceEntityForm",
 *       "edit" = "Drupal\uw_budget_calculator\Form\BudgetResidenceEntityForm",
 *       "delete" = "Drupal\uw_budget_calculator\Form\BudgetResidenceEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_budget_calculator\BudgetResidenceEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\uw_budget_calculator\BudgetResidenceEntityAccessControlHandler",
 *   },
 *   base_table = "budget_residence_entity",
 *   translatable = FALSE,
 *   admin_permission = "administer budget residence entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "canonical" = "/admin/mur/budget_calculator/budget_residence_entity/{budget_residence_entity}",
 *     "add-form" = "/admin/mur/budget_calculator/budget_residence_entity/add",
 *     "edit-form" = "/admin/mur/budget_calculator/budget_residence_entity/{budget_residence_entity}/edit",
 *     "delete-form" = "/admin/mur/budget_calculator/budget_residence_entity/{budget_residence_entity}/delete",
 *     "collection" = "/admin/mur/budget_calculator/budget_residence_entity",
 *   },
 *   field_ui_base_route = "budget_residence_entity.settings"
 * )
 */
class BudgetResidenceEntity extends ContentEntityBase implements BudgetResidenceEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Weight getter.
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * Weight setter.
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Residence entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Residence is published.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 100,
      ])
      ->setDescription(t('Display order weight.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
