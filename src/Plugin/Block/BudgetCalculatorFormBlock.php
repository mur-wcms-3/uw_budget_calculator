<?php

namespace Drupal\uw_budget_calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'BudgetCalculatorFormBlock' block.
 *
 * @Block(
 *  id = "budget_calculator_form_block",
 *  admin_label = @Translation("Budget Calculator"),
 *  category = @Translation("UW MUR"),
 * )
 */
class BudgetCalculatorFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * Class constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    State $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['content'] = [
      '#theme' => 'budget_calculator_form_block',
      '#programs' => json_encode($this->getPrograms(), TRUE),
      '#residences' => json_encode($this->getResidences(), TRUE),
      '#settings' => $this->getSettings(),
    ];

    return $build;
  }

  /**
   * Returns a list of available programs and their tuition values.
   *
   * @return array
   *   Returns programs.
   */
  private function getPrograms() {
    // Lookup programs.
    $pids = $this->entityTypeManager->getStorage('budget_program_entity')->getQuery()
      ->sort('name', 'ASC')
      ->execute();
    $entities = $this->entityTypeManager->getStorage('budget_program_entity')->loadMultiple($pids);

    $programs = [];
    foreach ($entities as $program) {
      $programs[] =
        [
          'id' => $program->id(),
          'name' => $program->name->getString(),
          'books' => $program->field_book_costs->getString(),
          'coop' => $program->field_co_op_fee->getString(),
          'domestic' => $program->field_domestic_tuition->getString(),
          'international' => $program->field_international_tuition->getString(),
          'incidental' => $program->field_incidental->getString(),
          'notes' => $program->field_notes->getString(),
        ];
    }

    return $programs;
  }

  /**
   * Returns a list of available residences and associated mealplans.
   *
   * @return array
   *   Returns residences.
   */
  private function getResidences() {
    // Lookup residences.
    $pids = $this->entityTypeManager->getStorage('budget_residence_entity')->getQuery()
      ->sort('weight', 'ASC')
      ->sort('name', 'ASC')
      ->execute();
    $entities = $this->entityTypeManager->getStorage('budget_residence_entity')->loadMultiple($pids);

    $residences = [];
    foreach ($entities as $residence) {

      $mids = $this->entityTypeManager->getStorage('budget_mealplan_entity')->getQuery()
        ->condition('field_residences.target_id', $residence->id())
        ->execute();

      $mealplan_entities = $this->entityTypeManager->getStorage('budget_mealplan_entity')->loadMultiple($mids);
      $mealplans = [];
      foreach ($mealplan_entities as $mealplan) {
        $mealplans[] = [
          'name' => $mealplan->name->value,
          'price' => $mealplan->field_price->value,
          'notes' => $mealplan->field_notes->value,
        ];
      }
      $residences[$residence->id()] = [
        'id' => $residence->id(),
        'name' => $residence->name->value,
        'price' => $residence->field_price->value,
        'mealplans' => $mealplans,
      ];
    }

    return $residences;
  }

  /**
   * Returns an array of settings to pass the template.
   *
   * @return array
   *   Returns settings.
   */
  private function getSettings($prefix = NULL) {

    $prefix = $prefix ?: 'uw_budget_calculator';
    $keys = [
      'tuition_heading_label',
      'tuition_header',
      'tuition_dropdown_label',
      'tuition_input_label',
      'books_input_label',
      'tuition_footer',

      'housing_heading_label',
      'housing_header',
      'location_input_label',
      'residence_heading_label',
      'residence_dropdown_label',
      'residence_input_label',
      'mealplan_dropdown_label',
      'mealplan_input_label',
      'residence_header',
      'rent_input_label',
      'utilities_input_label',
      'groceries_input_label',
      'off_campus_header',
      'off_campus_heading_label',
      'home_heading_label',
      'home_header',
      'home_input_label',

      'expenses_heading_label',
      'expenses_header',
      'weekly_heading',
      'entertainment_input_label',
      'monthly_heading',
      'phone_input_label',
      'personal_input_label',
      'shopping_input_label',
      'travel_input_label',
      'insurance_input_label',
      'other_input_label',
      'parking_input_label',
    ];

    $settings = [];
    foreach ($keys as $key) {
      $settings[$key] = $this->state->get("$prefix.$key");
    }

    return $settings;
  }

}
