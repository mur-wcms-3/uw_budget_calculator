<?php

namespace Drupal\uw_budget_calculator;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;

/**
 * Defines a class to build a listing of Budget Mealplan entities.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetMealplanEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['price'] = $this->t('Price');
    $header['field_notes'] = $this->t('Notes');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.budget_mealplan_entity.edit_form',
      ['budget_mealplan_entity' => $entity->id()]
    );
    $row['price'] = "$ " . $entity->field_price->value;
    $row['field_notes'] = Markup::create($entity->field_notes->value);
    $row['weight'] = $entity->getWeight();
    return $row + parent::buildRow($entity);
  }

}
