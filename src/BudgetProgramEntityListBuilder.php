<?php

namespace Drupal\uw_budget_calculator;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Budget Program entities.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetProgramEntityListBuilder extends EntityListBuilder {

  /**
   * Get entity ids.
   */
  protected function getEntityIds() {
    $query = $this->getStorage('budget_program_entity')->getQuery()
      ->sort('name');

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Program');
    $header['domestic'] = $this->t('Domestic Tuition');
    $header['international'] = $this->t('International Tuition');
    $header['field_incidental'] = $this->t('Incidental');
    $header['field_co_op_fee'] = $this->t('Co-op Fees');
    $header['field_book_costs'] = $this->t('Book Costs');
    $header['field_notes'] = $this->t('Notes');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.budget_program_entity.edit_form',
      ['budget_program_entity' => $entity->id()]
    );
    $row['domestic'] = "$ " . $entity->field_domestic_tuition->value;
    $row['international'] = "$ " . $entity->field_international_tuition->value;
    $row['field_incidental'] = "$ " . $entity->field_incidental->value;
    $row['field_co_op_fee'] = "$ " . $entity->field_co_op_fee->value;
    $row['field_book_costs'] = "$ " . $entity->field_book_costs->value;
    $row['field_notes'] = $entity->field_notes->value;

    return $row + parent::buildRow($entity);
  }

}
