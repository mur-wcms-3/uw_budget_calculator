<?php

namespace Drupal\uw_budget_calculator;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Budget Mealplan entity.
 *
 * @see \Drupal\uw_budget_calculator\Entity\BudgetMealplanEntity.
 */
class BudgetMealplanEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\uw_budget_calculator\Entity\BudgetMealplanEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished budget mealplan entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published budget mealplan entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit budget mealplan entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete budget mealplan entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add budget mealplan entities');
  }

}
