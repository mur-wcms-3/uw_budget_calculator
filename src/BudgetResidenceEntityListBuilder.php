<?php

namespace Drupal\uw_budget_calculator;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Budget Residence entities.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetResidenceEntityListBuilder extends EntityListBuilder {

  /**
   * Get entity ids.
   */
  protected function getEntityIds() {
    $query = $this->getStorage('budget_residence_entity')->getQuery()
      ->sort('weight', 'asc')
      ->sort('name', 'asc');

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['price'] = $this->t('Price');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\uw_budget_calculator\Entity\BudgetResidenceEntity $entity */
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.budget_residence_entity.edit_form',
      ['budget_residence_entity' => $entity->id()]
    );
    $row['price'] = "$ " . $entity->field_price->value;
    $row['weight'] = $entity->weight->value;
    return $row + parent::buildRow($entity);
  }

}
