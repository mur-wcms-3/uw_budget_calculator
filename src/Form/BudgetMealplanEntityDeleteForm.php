<?php

namespace Drupal\uw_budget_calculator\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Budget Mealplan entities.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetMealplanEntityDeleteForm extends ContentEntityDeleteForm {


}
