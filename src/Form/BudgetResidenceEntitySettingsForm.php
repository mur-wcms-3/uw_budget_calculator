<?php

namespace Drupal\uw_budget_calculator\Form;

use Drupal\core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Budget Residence Entity SettingsForm.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetResidenceEntitySettingsForm extends FormBase {
  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Fixture service.
   *
   * @var Drupal\uw_budget_calculator\FixtureService
   */
  protected $fixture;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->fixture = $container->get('uw_budget_calculator.fixtures');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'budgetresidenceentity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * Defines the settings form for Budget Residence entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['defaults'] = [
      '#markup' => '<p>Click the button below to populate drupal with the default set of residences.</p>',
    ];

    $form['create_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Residences'),
      '#submit' => ['::createDefaults'],
    ];

    $form['delete_markup'] = [
      '#markup' => '<p>Click the button below to clear ALL existing residence entities.</p>',
    ];

    $form['delete_entities'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ALL Residences'),
      '#submit' => ['::deleteEntities'],
    ];

    return $form;
  }

  /**
   * Calls the custom fixture service and installs default set of residences.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function createDefaults(array &$form, FormStateInterface $form_state) {
    // Call method to create the residences.
    $this->fixture->createDefaultResidences();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default residence values have been populated.'));
  }

  /**
   * Deletes entities.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteEntities(array &$form, FormStateInterface $form_state) {
    // Redirect to entity uninstall route.
    $url = Url::fromRoute('system.prepare_modules_entity_uninstall', ['entity_type_id' => 'budget_residence_entity']);
    $form_state->setRedirectUrl($url);
  }

}
