<?php

namespace Drupal\uw_budget_calculator\Form;

use Drupal\core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Budget Program Entity Settings Form.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetProgramEntitySettingsForm extends FormBase {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Fixture service.
   *
   * @var Drupal\uw_budget_calculator\FixtureService
   */
  protected $fixture;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->fixture = $container->get('uw_budget_calculator.fixtures');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'budgetprogramentity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * Defines the settings form for Budget Program entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['defaults'] = [
      '#markup' => '<p>Click the button below to populate drupal with the default set of programs.</p>',
    ];

    $form['create_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Programs'),
      '#submit' => ['::createDefaults'],
    ];

    $form['delete_markup'] = [
      '#markup' => '<p>Click the button below to clear ALL existing program entities.</p>',
    ];

    $form['delete_entities'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ALL Programs'),
      '#submit' => ['::deleteEntities'],
    ];

    return $form;
  }

  /**
   * Populate the default set of programs.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function createDefaults(array &$form, FormStateInterface $form_state) {
    // Call method to create the systems.
    $this->fixture->createDefaultPrograms();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default program values have been populated.'));
  }

  /**
   * Delete entities.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteEntities(array &$form, FormStateInterface $form_state) {
    // Redirect to entity uninstall route.
    $url = Url::fromRoute('system.prepare_modules_entity_uninstall', ['entity_type_id' => 'budget_program_entity']);
    $form_state->setRedirectUrl($url);
  }

}
