<?php

namespace Drupal\uw_budget_calculator\Form;

use Drupal\core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Budget Mealplan Entity Settings Form.
 *
 * @ingroup uw_budget_calculator
 */
class BudgetMealplanEntitySettingsForm extends FormBase {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Fixture service.
   *
   * @var Drupal\uw_budget_calculator\FixtureService
   */
  protected $fixture;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->fixture = $container->get('uw_budget_calculator.fixtures');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'budgetmealplanentity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * Defines the settings form for Budget Mealplan entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['defaults'] = [
      '#markup' => '<p>Click the button below to populate drupal with the default set of mealplans.</p>',
    ];

    $form['create_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Mealplans'),
      '#submit' => ['::createDefaults'],
    ];

    $form['relationship_markup'] = [
      '#markup' => '<p>Click the button below to populate drupal with the default set of mealplan relationships.</p>
                    <p><em><strong>NOTE:</strong> Ensure that the Residence Entities have been populated before running this command.</em></em></p>',
    ];

    $form['create_relationships'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Relationships'),
      '#submit' => ['::createRelationships'],
    ];

    $form['delete_markup'] = [
      '#markup' => '<p>Click the button below to clear ALL existing mealplan entities.</p>',
    ];

    $form['delete_entities'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ALL Mealplans'),
      '#submit' => ['::deleteEntities'],
    ];

    return $form;
  }

  /**
   * Populate default set of mealplans in to drupal's entity database.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function createDefaults(array &$form, FormStateInterface $form_state) {
    // Call method to create the mealplans.
    $this->fixture->createDefaultMealplans();
    // Display a message on completion.
    $this->messenger()->addStatus($this->t('Default mealplan values have been populated.'));
  }

  /**
   * Populate default set of mealplan relationships.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function createRelationships(array &$form, FormStateInterface $form_state) {
    // Call method to create the mealplans.
    $this->fixture->createDefaultRelationships();
    // Display a message on completion.
    $this->messenger()->addStatus($this->t('Default mealplan relationship values have been populated.'));
  }

  /**
   * Delete entities.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteEntities(array &$form, FormStateInterface $form_state) {
    // Redirect to entity uninstall route.
    $url = Url::fromRoute('system.prepare_modules_entity_uninstall', ['entity_type_id' => 'budget_mealplan_entity']);
    $form_state->setRedirectUrl($url);
  }

}
