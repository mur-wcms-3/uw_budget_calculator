<?php

namespace Drupal\uw_budget_calculator\Form;

use Drupal\Core\Link;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\State;

/**
 * Class Settings Form.
 */
class SettingsForm extends FormBase {

  /**
   * Configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, State $state) {
    $this->config = $configFactory->getEditable('uw_budget_calculator.settings');
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_budget_calculator_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#markup' => '<h2>Budget Calculator Section Settings</h2>',
    ];

    // Vertical tabs.
    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-tuition',
    ];

    // Tuition heading and content.
    $form['tuition'] = [
      '#type' => 'details',
      '#title' => $this->t('Tuition'),
      '#group' => 'tabs',
    ];

    $tuition_heading_label = $this->state->get('uw_budget_calculator.tuition_heading_label');
    $form['tuition']['tuition_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading Label'),
      '#default_value' => $tuition_heading_label,
    ];

    $tuition_header = $this->state->get('uw_budget_calculator.tuition_header');
    $form['tuition']['tuition_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Section Header'),
      '#default_value' => $tuition_header,
    ];

    $tuition_dropdown_label = $this->state->get('uw_budget_calculator.tuition_dropdown_label');
    $form['tuition']['tuition_dropdown_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tuition Dropdown Label'),
      '#default_value' => $tuition_dropdown_label,
    ];

    $link = Link::createFromRoute('Edit Tuition Dropdown Options', 'entity.budget_program_entity.collection')->toString();
    $form['tuition']['link'] = [
      '#markup' => "<p>$link</p>",
    ];

    $tuition_input_label = $this->state->get('uw_budget_calculator.tuition_input_label');
    $form['tuition']['tuition_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tuition Input Label'),
      '#default_value' => $tuition_input_label,
    ];

    $books_input_label = $this->state->get('uw_budget_calculator.books_input_label');
    $form['tuition']['books_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Books Input Label'),
      '#default_value' => $books_input_label,
    ];

    $tuition_footer = $this->state->get('uw_budget_calculator.tuition_footer');
    $form['tuition']['tuition_footer'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Section Footer'),
      '#default_value' => $tuition_footer,
    ];

    // Housing heading and content.
    $form['housing'] = [
      '#type' => 'details',
      '#title' => $this->t('Housing'),
      '#group' => 'tabs',
    ];

    $housing_heading_label = $this->state->get('uw_budget_calculator.housing_heading_label');
    $form['housing']['housing_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading Label'),
      '#default_value' => $housing_heading_label,
    ];

    $housing_header = $this->state->get('uw_budget_calculator.housing_header');
    $form['housing']['housing_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Section Header'),
      '#default_value' => $housing_header,
    ];

    $location_input_label = $this->state->get('uw_budget_calculator.location_input_label');
    $form['housing']['location_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location Radio Select Label'),
      '#default_value' => $location_input_label,
    ];

    // Res.
    $form['housing']['residence'] = [
      '#markup' => "<br><h3>Residence</h3><hr>",
    ];

    $residence_heading_label = $this->state->get('uw_budget_calculator.residence_heading_label');
    $form['housing']['residence_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Residence Heading Label'),
      '#default_value' => $residence_heading_label,
    ];

    $residence_header = $this->state->get('uw_budget_calculator.residence_header');
    $form['housing']['residence_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Residence Header Content'),
      '#default_value' => $residence_header,
    ];

    $residence_dropdown_label = $this->state->get('uw_budget_calculator.residence_dropdown_label');
    $form['housing']['residence_dropdown_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Residence Dropdown Label'),
      '#default_value' => $residence_dropdown_label,
    ];

    $residence_input_label = $this->state->get('uw_budget_calculator.residence_input_label');
    $form['housing']['residence_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Residence Input Label'),
      '#default_value' => $residence_input_label,
    ];

    $mealplan_dropdown_label = $this->state->get('uw_budget_calculator.mealplan_dropdown_label');
    $form['housing']['mealplan_dropdown_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meal plan Dropdown Label'),
      '#default_value' => $mealplan_dropdown_label,
    ];

    $mealplan_input_label = $this->state->get('uw_budget_calculator.mealplan_input_label');
    $form['housing']['mealplan_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meal plan Input Label'),
      '#default_value' => $mealplan_input_label,
    ];

    // Off campus.
    $form['housing']['off-campus'] = [
      '#markup' => "<br><h3>Off Campus</h3><hr>",
    ];

    $off_campus_heading_label = $this->state->get('uw_budget_calculator.off_campus_heading_label');
    $form['housing']['off_campus_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Off Campus Heading Label'),
      '#default_value' => $off_campus_heading_label,
    ];

    $off_campus_header = $this->state->get('uw_budget_calculator.off_campus_header');
    $form['housing']['off_campus_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Off Campus Header Content'),
      '#default_value' => $off_campus_header,
    ];

    $rent_input_label = $this->state->get('uw_budget_calculator.rent_input_label');
    $form['housing']['rent_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rent Input Label'),
      '#default_value' => $rent_input_label,
    ];

    $utilities_input_label = $this->state->get('uw_budget_calculator.utilities_input_label');
    $form['housing']['utilities_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Utilities Input Label'),
      '#default_value' => $utilities_input_label,
    ];

    $groceries_input_label = $this->state->get('uw_budget_calculator.groceries_input_label');
    $form['housing']['groceries_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Groceries Input Label'),
      '#default_value' => $groceries_input_label,
    ];

    $form['housing']['home'] = [
      '#markup' => "<br><h3>Living at home</h3><hr>",
    ];

    $home_heading_label = $this->state->get('uw_budget_calculator.home_heading_label');
    $form['housing']['home_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Living at home header label'),
      '#default_value' => $home_heading_label,
    ];

    $home_header = $this->state->get('uw_budget_calculator.home_header');
    $form['housing']['home_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Living at home header content'),
      '#default_value' => $home_header,
    ];

    $home_input_label = $this->state->get('uw_budget_calculator.home_input_label');
    $form['housing']['home_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Living at home header label'),
      '#default_value' => $home_input_label,
    ];

    // Expense content.
    $form['expenses'] = [
      '#type' => 'details',
      '#title' => $this->t('Expenses'),
      '#group' => 'tabs',
    ];

    $expenses_heading_label = $this->state->get('uw_budget_calculator.expenses_heading_label');
    $form['expenses']['expenses_heading_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heading Label'),
      '#default_value' => $expenses_heading_label,
    ];

    $expenses_header = $this->state->get('uw_budget_calculator.expenses_header');
    $form['expenses']['expenses_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Header Content'),
      '#default_value' => $expenses_header,
    ];

    $weekly_heading = $this->state->get('uw_budget_calculator.weekly_heading');
    $form['expenses']['weekly_heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Weekly Input Label'),
      '#default_value' => $weekly_heading,
    ];

    $entertainment_input_label = $this->state->get('uw_budget_calculator.entertainment_input_label');
    $form['expenses']['entertainment_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entertainment Input Label'),
      '#default_value' => $entertainment_input_label,
    ];

    $monthly_heading = $this->state->get('uw_budget_calculator.monthly_heading');
    $form['expenses']['monthly_heading'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Monthly Input Label'),
      '#default_value' => $monthly_heading,
    ];

    $phone_input_label = $this->state->get('uw_budget_calculator.phone_input_label');
    $form['expenses']['phone_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Input Label'),
      '#default_value' => $phone_input_label,
    ];

    $personal_input_label = $this->state->get('uw_budget_calculator.personal_input_label');
    $form['expenses']['personal_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Personal Care Input Label'),
      '#default_value' => $personal_input_label,
    ];

    $shopping_input_label = $this->state->get('uw_budget_calculator.shopping_input_label');
    $form['expenses']['shopping_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shopping Input Label'),
      '#default_value' => $shopping_input_label,
    ];

    $travel_input_label = $this->state->get('uw_budget_calculator.travel_input_label');
    $form['expenses']['travel_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Travel Input Label'),
      '#default_value' => $travel_input_label,
    ];

    $insurance_input_label = $this->state->get('uw_budget_calculator.insurance_input_label');
    $form['expenses']['insurance_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insurance Input Label'),
      '#default_value' => $insurance_input_label,
    ];

    $other_input_label = $this->state->get('uw_budget_calculator.other_input_label');
    $form['expenses']['other_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Other Input Label'),
      '#default_value' => $other_input_label,
    ];

    $parking_input_label = $this->state->get('uw_budget_calculator.parking_input_label');
    $form['expenses']['parking_input_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parking Input Label'),
      '#default_value' => $parking_input_label,
    ];

    // Resources content.
    $form['resources'] = [
      '#type' => 'details',
      '#title' => $this->t('Resources'),
      '#group' => 'tabs',
    ];

    // Submit Action.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Changes'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save Tuition Section Settings.
    $tuition_heading_label = $form_state->getValue('tuition_heading_label');
    $this->state->set('uw_budget_calculator.tuition_heading_label', $tuition_heading_label);

    $tuition_header = $form_state->getValue('tuition_header');
    $this->state->set('uw_budget_calculator.tuition_header', $tuition_header['value']);

    $tuition_dropdown_label = $form_state->getValue('tuition_dropdown_label');
    $this->state->set('uw_budget_calculator.tuition_dropdown_label', $tuition_dropdown_label);

    $tuition_input_label = $form_state->getValue('tuition_input_label');
    $this->state->set('uw_budget_calculator.tuition_input_label', $tuition_input_label);

    $books_input_label = $form_state->getValue('books_input_label');
    $this->state->set('uw_budget_calculator.books_input_label', $books_input_label);

    $tuition_footer = $form_state->getValue('tuition_footer');
    $this->state->set('uw_budget_calculator.tuition_footer', $tuition_footer['value']);

    // Save Housing Section Settings.
    $housing_heading_label = $form_state->getValue('housing_heading_label');
    $this->state->set('uw_budget_calculator.housing_heading_label', $housing_heading_label);

    $housing_header = $form_state->getValue('housing_header');
    $this->state->set('uw_budget_calculator.housing_header', $housing_header['value']);

    $location_input_label = $form_state->getValue('location_input_label');
    $this->state->set('uw_budget_calculator.location_input_label', $location_input_label);

    $residence_heading_label = $form_state->getValue('residence_heading_label');
    $this->state->set('uw_budget_calculator.residence_heading_label', $residence_heading_label);

    $residence_header = $form_state->getValue('residence_header');
    $this->state->set('uw_budget_calculator.residence_header', $residence_header['value']);

    $residence_dropdown_label = $form_state->getValue('residence_dropdown_label');
    $this->state->set('uw_budget_calculator.residence_dropdown_label', $residence_dropdown_label);

    $residence_input_label = $form_state->getValue('residence_input_label');
    $this->state->set('uw_budget_calculator.residence_input_label', $residence_input_label);

    $mealplan_dropdown_label = $form_state->getValue('mealplan_dropdown_label');
    $this->state->set('uw_budget_calculator.mealplan_dropdown_label', $mealplan_dropdown_label);

    $mealplan_input_label = $form_state->getValue('mealplan_input_label');
    $this->state->set('uw_budget_calculator.mealplan_input_label', $mealplan_input_label);

    $off_campus_heading_label = $form_state->getValue('off_campus_heading_label');
    $this->state->set('uw_budget_calculator.off_campus_heading_label', $off_campus_heading_label);

    $off_campus_header = $form_state->getValue('off_campus_header');
    $this->state->set('uw_budget_calculator.off_campus_header', $off_campus_header['value']);

    $rent_input_label = $form_state->getValue('rent_input_label');
    $this->state->set('uw_budget_calculator.rent_input_label', $rent_input_label);

    $utilities_input_label = $form_state->getValue('utilities_input_label');
    $this->state->set('uw_budget_calculator.utilities_input_label', $utilities_input_label);

    $groceries_input_label = $form_state->getValue('groceries_input_label');
    $this->state->set('uw_budget_calculator.groceries_input_label', $groceries_input_label);

    $home_heading_label = $form_state->getValue('home_heading_label');
    $this->state->set('uw_budget_calculator.home_heading_label', $home_heading_label);

    $home_header = $form_state->getValue('home_header');
    $this->state->set('uw_budget_calculator.home_header', $home_header['value']);

    $home_input_label = $form_state->getValue('home_input_label');
    $this->state->set('uw_budget_calculator.home_input_label', $home_input_label);

    $expenses_heading_label = $form_state->getValue('expenses_heading_label');
    $this->state->set('uw_budget_calculator.expenses_heading_label', $expenses_heading_label);

    $expenses_header = $form_state->getValue('expenses_header');
    $this->state->set('uw_budget_calculator.expenses_header', $expenses_header['value']);

    $weekly_heading = $form_state->getValue('weekly_heading');
    $this->state->set('uw_budget_calculator.weekly_heading', $weekly_heading);

    $entertainment_input_label = $form_state->getValue('entertainment_input_label');
    $this->state->set('uw_budget_calculator.entertainment_input_label', $entertainment_input_label);

    $monthly_heading = $form_state->getValue('monthly_heading');
    $this->state->set('uw_budget_calculator.monthly_heading', $monthly_heading);

    $phone_input_label = $form_state->getValue('phone_input_label');
    $this->state->set('uw_budget_calculator.phone_input_label', $phone_input_label);

    $personal_input_label = $form_state->getValue('personal_input_label');
    $this->state->set('uw_budget_calculator.personal_input_label', $personal_input_label);

    $shopping_input_label = $form_state->getValue('shopping_input_label');
    $this->state->set('uw_budget_calculator.shopping_input_label', $shopping_input_label);

    $travel_input_label = $form_state->getValue('travel_input_label');
    $this->state->set('uw_budget_calculator.travel_input_label', $travel_input_label);

    $insurance_input_label = $form_state->getValue('insurance_input_label');
    $this->state->set('uw_budget_calculator.insurance_input_label', $insurance_input_label);

    $other_input_label = $form_state->getValue('other_input_label');
    $this->state->set('uw_budget_calculator.other_input_label', $other_input_label);

    $parking_input_label = $form_state->getValue('parking_input_label');
    $this->state->set('uw_budget_calculator.parking_input_label', $parking_input_label);

    // Save our setting changes.
    $this->config->save();
  }

}
