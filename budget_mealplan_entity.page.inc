<?php

/**
 * @file
 * Contains budget_mealplan_entity.page.inc.
 *
 * Page callback for Budget Mealplan entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Budget Mealplan templates.
 *
 * Default template: budget_mealplan_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_budget_mealplan_entity(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
