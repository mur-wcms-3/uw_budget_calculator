<?php

/**
 * @file
 * Contains budget_program_entity.page.inc.
 *
 * Page callback for Budget Program entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Budget Program templates.
 *
 * Default template: budget_program_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_budget_program_entity(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
